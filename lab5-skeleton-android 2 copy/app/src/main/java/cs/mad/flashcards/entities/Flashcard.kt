package cs.mad.flashcards.entities

import androidx.room.*


@Entity
data class Flashcard(@PrimaryKey val id: Int?, val question: String, val answer: String) {
    //needs a primary key
    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            val cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard(null,"Term $i", "Def $i"))
            }
            return cards
        }
    }
}

@Dao
interface flashcardInterface {

    @Query("Select * from Flashcard") //Select EVERYTHING from table. Every row from table.
    suspend fun getAll(): List<Flashcard>
    //suspend = code can be paused for other code to work // asynchronous programming
    // must be called within a co-routine.

    @Insert
    suspend fun insert(flashInsert: Flashcard)

    @Update
    suspend fun update(flashUpdate:Flashcard)

    @Delete
    suspend fun delete(flashDelete:Flashcard)


}