package cs.mad.flashcards.activities

import android.media.CamcorderProfile.getAll
import android.os.Bundle
import android.provider.SyncStateContract.Helpers.insert
import android.provider.SyncStateContract.Helpers.update
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.flashDatabase
import cs.mad.flashcards.runOnIO
import java.nio.file.Files.delete

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)





        val db = Room.databaseBuilder( //gets the reference to the database
            applicationContext,
            flashDatabase::class.java, flashDatabase.databaseName
        ).build() //builds the database





        var flashSetDao = db.flashcardSetDao()
        var flSetDAO = listOf<FlashcardSet>()
            runOnIO {
                flSetDAO = flashSetDao.getAll()
            }


        // create instance of database, from that instance get a reference to dao,
        // from dao call the getAll function from database


        binding.flashcardSetList.adapter = FlashcardSetAdapter(flSetDAO) //needs list of flashcard sets

        binding.createSetButton.setOnClickListener {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(FlashcardSet("test"))
            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)



            runOnIO {
                flashSetDao.insert(flashcardSetInsert = FlashcardSet("insert"))
            }
        }
    }
}

//should I be clicking a button and loading the
//--> data to the database?