package cs.mad.flashcards.entities

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [FlashcardSet::class, Flashcard::class],  version = 1)
abstract class flashDatabase : RoomDatabase()  {

    companion object {
        const val databaseName = "FLASHCARD_DATABASE"
    }
    abstract fun flashcardSetDao(): flashcardSetInterface
    //Basically gets an instance of interface so that we can call them

    abstract fun flashcardDao(): flashcardInterface

}



