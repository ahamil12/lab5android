package cs.mad.flashcards.entities

import android.graphics.Color
import androidx.room.*


@Entity
data class FlashcardSet(val title: String, @PrimaryKey var id: Long? = null) {
    companion object {
        fun getHardcodedFlashcardSets(): List<FlashcardSet> {
            val sets = mutableListOf<FlashcardSet>()
            for (i in 1..10) {
                sets.add(FlashcardSet("Set $i"))
            }
            return sets
        }
    }
}


@Dao
interface flashcardSetInterface {

    @Query("Select * from FlashcardSet")
    suspend fun getAll(): List<FlashcardSet>

    @Insert
    suspend fun insert(flashcardSetInsert: FlashcardSet)

    @Delete
    suspend fun delete(flashcardSetDelete:FlashcardSet)


//flashcardSet = @query, @insert



    // Database interaction functions like .get, .insert, etc. so "inserts"
    // One function per functionality
    // all needing different annotations
    // create my functions here

}
//DAO = data access objects is where you will define database functions
//Entity = signals to database this will be a table in the database