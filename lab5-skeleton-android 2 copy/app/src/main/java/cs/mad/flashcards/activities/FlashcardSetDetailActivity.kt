package cs.mad.flashcards.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.flashDatabase
import cs.mad.flashcards.runOnIO

class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)




        val db = Room.databaseBuilder( //gets the reference to the database
            applicationContext,
            flashDatabase::class.java, flashDatabase.databaseName
        ).build()




        var flashcardDao = db.flashcardDao()
        var flashcardSetDao = db.flashcardSetDao()
        var flcardDAO = listOf<Flashcard>()



        runOnIO {
            flcardDAO = flashcardDao.getAll()
        }


        binding.flashcardList.adapter = FlashcardAdapter(flcardDAO, flashcardDao)


        binding.addFlashcardButton.setOnClickListener {
            var itemss = Flashcard(null, "question", "answer")
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(itemss)
             binding.flashcardList.smoothScrollToPosition((binding.
             flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }



        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }



    }
}


//How do I pass data between Activities in Android application?